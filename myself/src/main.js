import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router'
import vant from 'vant'
import 'vant/lib/index.css';
import { Tabbar, TabbarItem } from 'vant';


Vue.use(vant).use(VueRouter)
Vue.use(Tabbar).use(TabbarItem);


Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
