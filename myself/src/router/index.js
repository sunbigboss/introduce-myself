import VueRouter from 'vue-router'

export default new VueRouter({
    routes:[{
        path:'/',name:'home',component:()=>import('../components/home')
    },{
        path:'/practice',name:'practice',component:()=>import('../components/practice')
    },{
        path:'/study',name:'study',component:()=>import('../components/study')
    },{
        path:'*',name:'error',component:()=>import('../components/error')
    }]
})